# AWS CloudHSM builder

This toolkit facilitates AWS CloudHSM deployments. It is heavily based on:

* Ansible
* Terraform
* AWS guidelines

It's an opinionated reference implementation of an IaC deployment of AWS CloudHSM.

## Requirements

The toolkit is based on the presence of an environment variable: `CLOUDHSM_CLUSTER` that points the name of the deployment you are working on. In the usage examples to come, please keep in mind that it should be defined.

## Step by Step

### Create a new workspace for your cloudhsm

* Run `make init-workspace` to:
  * create a directory at `./cloudhsm/${CLOUDHSM_CLUSTER}` where all work files for your deployment will be stored.
* **Edit `./cloudhsm/${CLOUDHSM_CLUSTER}/cluster.vars.yml` and adjust the configuration.**

### (Optional) Create a Security Operation Center for deployment

* Run `make open-ceremony` to:
  * terraform-apply the layer `./terraform/00-soc` that includes:
    * Networking (VPC, subnets, ...)
    * an EC2 instance for a security team member to operate the rest of the deployment procedure
  * synchronize your workspace on the fresh SOC instance.
  * Get the ssh connection command to the SOC instance.
* Connect to the SOC instance before following the next steps.

> Reverse is `make close-ceremony`

### Deploy the CloudHSM cluster

* Run `make hsm-create` to:
  * terraform-apply the layer `./terraform/01-cloudhsm-cluster` that includes:
    * Networking (VPC, subnets, ...)
    * CloudHSM cluster
    * Single HSM in the cluster

> Reverse is `make hsm-destroy`

### Initialize and activate the cluster

* Run `make hsm-configure` to :
  * terraform-apply the layer `./terraform/02-cloudhsm-configurator`, a jump host in the security group of the CloudHSM cluster
  * verify the identity of the HSM in the cluster
  * change default admin password according to the configuration taken from `./cloudhsm/${CLOUDHSM_CLUSTER}/cluster.vars.yml`
  * create a kms user for KMS integration
  * terraform-destroy the layer `./terraform/02-cloudhsm-configurator`

### Add HSM to your cluster

* Run `make hsm-upscale` to :
  * terraform-apply the layer `./terraform/03-cloudhsm-scaling`, that adds HSM hosts to your cluster. They are built as copy of the first one, synchronized both on keys content and users.

> Reverse is `make hsm-downscale`

### Create a KMS custom key store connected to your cluster

* Run `make kms-create-ks` to :
  * Create the custom key store
  * Connect it to your cluster

> Reverse is `make kms-destroy-ks`
