init-workspace:
	mkdir -p cloudhsm/${CLOUDHSM_CLUSTER}
	cp cloudhsm/sample.vars.yml cloudhsm/${CLOUDHSM_CLUSTER}/cluster.vars.yml

dependency:
	ansible-galaxy install -fr ansible/requirements.yml

ceremony-open: dependency
	ansible-playbook ansible/ceremony-open.yml -e @cloudhsm/${CLOUDHSM_CLUSTER}/cluster.vars.yml

ceremony-close:
	ansible-playbook ansible/ceremony-close.yml -e @cloudhsm/${CLOUDHSM_CLUSTER}/cluster.vars.yml

hsm-create:
	ansible-playbook ansible/hsm-create.yml -e @cloudhsm/${CLOUDHSM_CLUSTER}/cluster.vars.yml

hsm-destroy:
	ansible-playbook ansible/hsm-destroy.yml -e @cloudhsm/${CLOUDHSM_CLUSTER}/cluster.vars.yml

hsm-configure: dependency
	ansible-playbook ansible/hsm-configure.yml -e @cloudhsm/${CLOUDHSM_CLUSTER}/cluster.vars.yml

hsm-upscale:
	ansible-playbook ansible/hsm-upscale.yml -e @cloudhsm/${CLOUDHSM_CLUSTER}/cluster.vars.yml

hsm-downscale:
	ansible-playbook ansible/hsm-downscale.yml -e @cloudhsm/${CLOUDHSM_CLUSTER}/cluster.vars.yml

kms-create-ks:
	ansible-playbook ansible/kms-create-ks.yml -e @cloudhsm/${CLOUDHSM_CLUSTER}/cluster.vars.yml

kms-create-keys:
	ansible-playbook ansible/kms-create-keys.yml -e @cloudhsm/${CLOUDHSM_CLUSTER}/cluster.vars.yml

kms-destroy-ks:
	ansible-playbook ansible/kms-destroy-ks.yml -e @cloudhsm/${CLOUDHSM_CLUSTER}/cluster.vars.yml

hsm-save:
	ansible-playbook ansible/hsm-save.yml -e @cloudhsm/${CLOUDHSM_CLUSTER}/cluster.vars.yml




