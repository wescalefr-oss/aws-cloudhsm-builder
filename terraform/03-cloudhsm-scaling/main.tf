
resource "aws_cloudhsm_v2_hsm" "cloudhsm_v2_hsm" {
  count = length(var.target_subnet_ids)

  subnet_id  = element(var.target_subnet_ids, count.index)
  cluster_id = var.cloudhsm_cluster_id
}
