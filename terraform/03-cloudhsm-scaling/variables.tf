variable "aws_profile" {}

variable "cloudhsm_region" {}

variable "cloudhsm_cluster_id" {}

variable "target_subnet_ids" {
  type = list
}
