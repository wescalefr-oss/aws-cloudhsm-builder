
resource "aws_vpc" "soc" {
  cidr_block = "${local.vpc_cidr}"

  tags = {
    Name = "soc"
  }
}

data "aws_availability_zones" "available" {}

resource "aws_subnet" "cloudhsm2_subnets" {
  vpc_id     = "${aws_vpc.soc.id}"
  cidr_block = "${local.subnet_cidr}"

  map_public_ip_on_launch = false

  availability_zone = "${var.cloudhsm_region}a"

  tags = {
    Name = "hsm-proto"
  }
}


data "aws_ami" "centos" {
  most_recent = true

  filter {
    name   = "name"
    values = ["CentOS Linux 7 x86_64 HVM EBS*"]
  }

  owners = ["679593333241"]
}


resource "aws_key_pair" "instance_key" {
  key_name_prefix = "soc-master"
  public_key      = "${file(var.public_key_file)}"
}

resource "aws_security_group" "sg_app" {
  name        = "soc"
  description = "soc"
  vpc_id      = "${aws_vpc.soc.id}"

  tags = {
    Name = "soc"
  }
}

resource "aws_security_group_rule" "sg_ssh_in" {
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.sg_app.id}"
  type              = "ingress"
}

resource "aws_security_group_rule" "sg_all_out" {
  from_port         = 0
  to_port           = 0
  protocol          = "all"
  cidr_blocks       = ["0.0.0.0/0"]
  security_group_id = "${aws_security_group.sg_app.id}"
  type              = "egress"
}


resource "aws_iam_instance_profile" "soc" {
  name_prefix = "cloudhsm-soc-"
  role = "${aws_iam_role.role.name}"
}

resource "aws_iam_role" "role" {
  name = "soc-instance"
  path = "/"

  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Action": "sts:AssumeRole",
            "Principal": {
               "Service": "ec2.amazonaws.com"
            },
            "Effect": "Allow",
            "Sid": ""
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "soc_to_admin" {
  role = "${aws_iam_role.role.name}"
  policy_arn = "arn:aws:iam::aws:policy/AdministratorAccess"
}

resource "aws_instance" "devbox" {
  ami = "${data.aws_ami.centos.image_id}"
  instance_type = "t3.medium"
  iam_instance_profile = "${aws_iam_instance_profile.soc.name}"
  key_name = "${aws_key_pair.instance_key.key_name}"
  subnet_id = "${aws_subnet.cloudhsm2_subnets.id}"

  vpc_security_group_ids = [
    "${aws_security_group.sg_app.id}",
  ]

  associate_public_ip_address = true

  tags = {
    Name = "soc"
  }
}

resource "aws_internet_gateway" "gateway" {
  vpc_id = "${aws_vpc.soc.id}"
}

resource "aws_route_table" "main" {
  vpc_id = "${aws_vpc.soc.id}"
}

resource "aws_route" "main_to_everything" {
  route_table_id = "${aws_route_table.main.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id = "${aws_internet_gateway.gateway.id}"
}

resource "aws_main_route_table_association" "main" {
  route_table_id = "${aws_route_table.main.id}"
  vpc_id = "${aws_vpc.soc.id}"
}
