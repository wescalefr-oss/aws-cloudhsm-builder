variable "cloudhsm_region" {}
variable "public_key_file" {}

locals {
  vpc_cidr    = "10.42.0.0/16"
  subnet_cidr = "10.42.1.0/24"
}
