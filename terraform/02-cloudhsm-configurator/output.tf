output "own_ip" {
  value = data.http.ipify.body
}

output "ip" {
  value = aws_instance.jump_host.public_ip
}
