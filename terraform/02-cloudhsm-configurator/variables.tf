variable "aws_profile" {
}

variable "cloudhsm_vpc_id" {
}

variable "cloudhsm_region" {
}

variable "cloudhsm_cluster_dir" {
}

variable "target_subnet_id" {
}

variable "cloudhsm_cluster_id" {
}

variable "jump_host_instance_type" {
  default = "t2.medium"
}

locals {
  label = "cloudhsm-jump-host-${var.cloudhsm_cluster_id}"
}
