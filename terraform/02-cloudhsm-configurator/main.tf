
resource "tls_private_key" "default" {
  algorithm = "RSA"
  rsa_bits  = 4096
}

resource "local_file" "ssh_private_key" {

  content    = tls_private_key.default.private_key_pem
  filename   = "${var.cloudhsm_cluster_dir}/${local.label}.rsa"
}

resource "aws_key_pair" "instance_key" {
  key_name_prefix = local.label
  public_key      = tls_private_key.default.public_key_openssh
}

resource "aws_security_group" "sg_app" {
  name        = local.label
  description = local.label
  vpc_id      = var.cloudhsm_vpc_id

  tags = {
    Name = local.label
  }
}

resource "aws_security_group_rule" "ssh_in" {
  security_group_id = aws_security_group.sg_app.id

  type              = "ingress"
  protocol          = "tcp"
  from_port         = 22
  to_port           = 22
  
  cidr_blocks       = ["${data.http.ipify.body}/32"]
}

resource "aws_security_group_rule" "all_out" {
  security_group_id = aws_security_group.sg_app.id

  type              = "egress"
  protocol          = "all"
  from_port         = 0
  to_port           = 0
  
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_instance" "jump_host" {
  ami           = data.aws_ami.centos.image_id

  instance_type = var.jump_host_instance_type

  key_name = aws_key_pair.instance_key.key_name

  subnet_id = var.target_subnet_id

  vpc_security_group_ids = [
    aws_security_group.sg_app.id,
    data.aws_security_group.cloudhsm.id,
  ]

  associate_public_ip_address = true

  tags = {
    Name = local.label
  }
}

resource "aws_internet_gateway" "gateway" {
  vpc_id = var.cloudhsm_vpc_id
}

resource "aws_route_table" "main" {
  vpc_id = var.cloudhsm_vpc_id
}

resource "aws_route" "main_to_internet" {
  route_table_id         = aws_route_table.main.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.gateway.id
}

resource "aws_main_route_table_association" "main" {
  route_table_id = aws_route_table.main.id
  vpc_id         = var.cloudhsm_vpc_id
}
