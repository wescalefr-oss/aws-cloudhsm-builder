data "http" "ipify" {
  url = "https://api.ipify.org"
}

data "aws_ami" "centos" {
  most_recent = true

  filter {
    name   = "name"
    values = ["CentOS Linux 7 x86_64 HVM EBS*"]
  }

  owners = ["679593333241"]
}

data "aws_security_group" "cloudhsm" {
  name = "cloudhsm-${var.cloudhsm_cluster_id}-sg"
}
