resource "aws_vpc" "cloudhsm" {
  cidr_block = var.cloudhsm_vpc_cidr

  tags = {
    Name = var.cloudhsm_vpc_name
  }
}

resource "aws_subnet" "cloudhsm" {
  count = length(data.aws_availability_zones.available.names)

  vpc_id                  = aws_vpc.cloudhsm.id
  availability_zone       = element(data.aws_availability_zones.available.names, count.index)
  cidr_block              = cidrsubnet(var.cloudhsm_vpc_cidr, 8, count.index + 1)

  map_public_ip_on_launch = false
  
  tags = {
    Name = "${var.cloudhsm_vpc_name}_${count.index}"
  }
}

resource "aws_cloudhsm_v2_cluster" "cloudhsm" {
  hsm_type   = "hsm1.medium"
  subnet_ids = aws_subnet.cloudhsm.*.id

  tags = {
    Name = var.cloudhsm_vpc_name
  }
}

resource "aws_cloudhsm_v2_hsm" "cloudhsm" {
  subnet_id  = aws_subnet.cloudhsm[0].id
  cluster_id = aws_cloudhsm_v2_cluster.cloudhsm.id
}

data "aws_network_interface" "cloudhsm" {
  id = aws_cloudhsm_v2_hsm.cloudhsm.hsm_eni_id
}

