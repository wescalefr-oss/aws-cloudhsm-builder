output "cluster_id" {
  value = aws_cloudhsm_v2_cluster.cloudhsm.id
}

output "hsm_ip" {
  value = [
    data.aws_network_interface.cloudhsm.private_ip,
  ]
}

output "vpc_id" {
  value = aws_vpc.cloudhsm.id
}

output "subnets_ids" {
  value = aws_subnet.cloudhsm.*.id
}

output "cluster_state" {
  value = aws_cloudhsm_v2_cluster.cloudhsm.cluster_state
}

output "certificates" {
  value = aws_cloudhsm_v2_cluster.cloudhsm.cluster_certificates
}

