variable "aws_profile" {}

variable "cloudhsm_region" {}

variable "cloudhsm_vpc_cidr" {}

variable "cloudhsm_vpc_name" {}

data "aws_availability_zones" "available" {
  state = "available"
}
